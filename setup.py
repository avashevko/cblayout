"""
The following in the install requirements clash with conda installations.
So leave it be for the moment.

        'networkx',
        'seaborn',
        'python-igraph',
        'prettytable',
        'matplotlib',
        'fuzzywuzzy'
"""

from setuptools import setup

setup(name='cblayout',
    version='0.1',
    description='Cohesive Block Layout',
    author='Anthony Vashevko',
    author_email='avashevko@gmail.com',
    license='MIT',
    packages=['cblayout'],
    install_requires=[
      ],
  scripts=[],
  zip_safe=False)

# Hierarchical Network Layout

Network layout and plotting package for hierarchical networks, as described in
["Visualizing Hierarchical Social Networks"](about:blank), forthcoming in [Socius](http://journals.sagepub.com/home/srd). Use for visualizing
network analysis techniques that hierarchically decompose a network:

- [Cohesive blocking](https://www.jstor.org/stable/3088904)
- [*k*-core decomposition](https://arxiv.org/abs/cs/0504107)
- [*k*-shell decomposition](http://www.pnas.org/content/104/27/11150)


## Installation

`git clone` this repository, then install the package with `pip install -U
cblayout`.

### Requires

- python3


## Usage

This package plots a data structure containing a network and hierarchy
information about that network.
[github.com/balachia/kcore_tree](https://github.com/balachia/kcore_tree)
provides a sample implementation of *k*-core decomposition that produces such a
data structure.

The `examples` folder also provides sample data from cohesive blocking analysis
of the Zachary Karate Club (`zachary.pickle`), and a sample script for producing
a plot (`plot-zachary.py`).

In general, you can generate a plot by running:

```python
import cblayout as cbl

cbl.block_plot(block_file='zachary.pickle')
```

## License

The MIT License

Copyright (c) 2018, Kurtuluş Gemici and Anthony Vashevko 

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
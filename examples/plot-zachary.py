import cblayout as cbl

cbl.block_plot(block_file='zachary.pickle',
        title='Zachary\'s Karate Club: Cohesive Blocks',
        outfile='zachary.pdf',
        verbose=1)


import igraph as ig
from collections import deque, OrderedDict


def bfs_tree(G, root):
    """
    Build the reverse BFS traversal list. Generate parents dict and children attribute for each vertex.
    
    :param G: an iGraph Graph object
    :param root: the root of the iGraph tree
    :return: a tuple of (parent dict, traversal list)
    
    .. note:
        We return the traversal after reversing it through negative slicing.
        In other words, we return the reverse traversal order, direction is bottom up.
        The function has the side effect of generating a children attribute for each vertex.
    """
    # parent is a dictionary where key is a vertex and value is that vertex's parent
    parent = {G.vs[root]: None}
    # list to record the traversal order
    traversal = []
    # FIFO queue for the BFS traversal
    Q = deque([G.vs[root]])
    while Q:
        current = Q.popleft()
        # add this node to the traversal order
        traversal.append(current)
        # the set to record children
        children = set()
        for nghbr in current.neighbors():
            # if neighbor is in parents, just skip it
            if nghbr in parent: continue
            # otherwise the current node pointer is this
            # neighbor's parent
            parent[nghbr] = current
            # add this neighbor to the children set
            children.add(nghbr)
            # add the nghbr to the traversal queue
            Q.append(nghbr)
        # update the current node's children attribute
        current['children'] = list(children)
    # note that we return the traversal after reversing it through negative slicing
    return parent, traversal[::-1]

def dfs_tree(G, root):
    """
    Build the reverse DFS traversal list.
    
    :param G: an iGraph Graph object
    :param root: the root of the iGraph tree
    :return: reverse DFS traversal list
    
    .. note:
        We return the traversal after reversing it through negative slicing.
        In other words, we return the reverse traversal order, direction is bottom up.
    """
    # the set of visited nodes
    seen = set()
    # list to record the traversal order
    traversal = []
    # stack for the DFS traversal
    Q = []
    # start with root
    Q.append(G.vs[root])
    while Q:
        current = Q.pop()
        # skip if seen
        if current in seen: continue
        # otherwise now it's seen
        seen.add(current)
        # add all the neighbors to the stack
        Q.extend(current.neighbors())
        # add this node to the traversal order
        traversal.append(current)
    # note that we return the traversal after reversing it through negative slicing
    return traversal[::-1]

def hierarchy_size_walk(g, v0):
    """Calculate and records area assignable to children,
    then create a squarified treemap layout.

    :param g: igraph graph
    :param v0: root node
    """
    v = g.vs[v0]

    # record children
    children = [g.es[e].target for e in g.incident(v0)]
    v['children'] = children

    # calculate child sizes
    sizes = [v['self_size']]
    sizes.extend( [hierarchy_size_walk(g, c) for c in children] )
    v['sizes'] = sizes

    tree_size = sum(sizes)
    return tree_size

def hierarchy_size_walk_iter(hier, parent, traversal):
    """Calculate and record area assignable to children.
    
    Iterative version, to avoid Python recursive call limit.

    :param hier: igraph hierarchy graph, where each vertex is a block
    :param parent: a deque object of parents in the hierarchy graph
    :param traversal: a list encapsulating the BFS traversal for the hierarchy graph 
    :return: the size of the tree
    
    .. note:
        Adds a 'sizes' dict to each block in hierarchy--a dict of blk idx and size,
        iteratively updated with children sizes.
    """
    # we will make two passes over the tree
    # First add each block's self_size to its sizes_dict
    for v in traversal:
        v['sizes'] = OrderedDict({v['_idx']: v['self_size']})
    # traverse the BFS queue
    # each vertex has a single parent, except root
    # if a parent exists
    # we add the sum of that vertex's sizes dict to its parent
    # by extending the parent's sizes_dict
    # root self_size is not updated
    for v in traversal:
        if parent[v]:
            parent[v]['sizes'].update({v['_idx']: sum(v['sizes'].values())})

def get_initial_claims(hier):
    claims = dict()
    for v in hier.vs:
        claims[v['_idx']] = v['self_size']
    return claims

def get_subtree_claims(claims, traversal):
    stclaims = dict()
    for v in traversal:
        child_claims = sum([stclaims[c['_idx']] for c in v['children']])
        stclaims[v['_idx']] = claims[v['_idx']] + child_claims
    return stclaims

def assign_property_to_hierarchy(hier, blks):
    """Assign block properties to hierarchy.

    :param hier: iGraph hierarchy graph, where each vertex is a block
    :param blks: block objects from iGraph cohesive blocks analysis, defined in the igraph module
    """
    # this just passes the variables in each blk to hierarchy graph object 
    for blk in blks:
        hv = hier.vs[blk.idx]
        hv['_idx'] = blk.idx
        hv['_klevel'] = blk.klevel
        hv['_kconn'] = blk.kconn
        hv['parent'] = blk.parent

def assign_labels_to_hierarchy(hier):
    """Assigns labels to hierarchy nodes."""
    # ancestors = get_ancestors(hier)
    for hv in hier.vs:
        # hlevel = len(ancestors[hv['_idx']])
        # hv['label'] = '%s (k: %s)' % (hlevel, hv['_klevel'])
        hv['label'] = '%s (k: %s)' % (hv['_klevel'], hv['_kconn'])

def hierarchy_shapes(g, v0):
    """Extract shapes from a hierarchy graph, e.g. for plotting

    :param g: igraph graph
    :param v0: root node of subtree to extract
    """
    v = g.vs[v0]
    shapes = [v['_shape']]
    for c in v['children']:
        shapes.extend(hierarchy_shapes(g, c))
    return shapes

def get_ancestors(hierarchy):
    """Calculates ancestry lists for blocks in a hierarchy.

    Returns a dictionary mapping block ids to ancestry lists, in order from self to root.

    :param hierarchy: hierarchy graph with block properties attached
    :return: dict (block id -> ancestor list)
    """
    # get ancestry trees for each block
    ancestors = dict()
    for hv in hierarchy.vs:
        curr = hv['_idx']
        trail = []
        while curr is not None:
            trail.append(curr)
            curr = hierarchy.vs[curr]['parent']
            if curr in ancestors:
                trail.extend(ancestors[curr])
                curr = None
        ancestors[hv['_idx']] = trail
    return ancestors

def get_lca(i, j, ancestors):
    """Return the lowest common ancestor of two blocks (i.e. the point at which they diverge).

    :param i, j: two block ids
    :param ancestors: ancestry dict
    """
    anci = ancestors[i]
    ancj = ancestors[j]

    idx = -1
    depth = min(len(anci), len(ancj))
    while (idx+depth >= 0) and (anci[idx] == ancj[idx]):
        idx -= 1
    return anci[idx+1]


import numpy as np
import shapely.geometry as sg

from collections import defaultdict
from . hierarchy_tools import get_ancestors, get_lca

def ngrad(f, x, i, eps=1.0e-8):
    grad = np.zeros(x.shape[1])
    js = range(x.shape[1])

    for j in js:
        x0 = x[i, j]
        x[i, j] = x0 + eps
        fp = f(x, i)
        x[i, j] = x0 - eps
        fm = f(x, i)
        x[i, j] = x0
        
        # bounds check
        if np.isfinite(fp) and np.isfinite(fm):
            grad[j] += (fp - fm)/(2*eps)
            fcalls = 2
        else:
            f0 = f(x, i)
            fcalls = 3
            if np.isfinite(fp):
                grad[j] += (fp - f0)/eps
            elif np.isfinite(fm):
                grad[j] += (f0 - fm)/eps
            else:
                raise ValueError("No gradient at index {}: ({}, {}), f: (+{}, -{})".format(i, x[i,0], x[i,1], fp, fm))
    return grad

def get_graph_hierarchy_mapping(graph):
    # main graph <-> hierarchy mapping
    hierarchy_gvs = defaultdict(list)
    slice = []
    for gv in graph.vs:
        hierarchy_gvs[gv['block']].append(gv.index)
        slice.append(gv['block'])
    return (hierarchy_gvs, slice)

def get_hierarchy_distances(hierarchy):
    """Build matrix of inter-block distances.

    Uses centroid distance.

    :param hierarchy: hierarchy graph
    """
    # get block-block distances
    n = len(hierarchy.vs)
    hdists = np.zeros((n,n))

    # iterate over hierarchy elements
    for i in range(n):
        ispace = hierarchy.vs[i]['_self_space']
        ibx = None if ispace is None else ispace.bbox
        for j in range(i+1, n):
            # get bbox distance; diagonal default is 0 already
            jspace = hierarchy.vs[j]['_self_space']
            if ispace is None or jspace is None:
                hdists[i, j] = 0
            jbx = jspace.bbox

            dist = np.sqrt((jbx[2]-jbx[0] - ibx[2]+ibx[0])**2 + (jbx[3]-jbx[1] - ibx[3]+ibx[1])**2)
            hdists[i, j] = dist
            hdists[j, i] = dist

    return hdists


def energy_shape_factory(graph, hierarchy, **kwargs):
    """Builds an energy function pushing nodes away from shape boundaries.

    :param graph: igraph graph with v['_shape'] parameter
    :return: energy function
    """
    # energy = \sum_i log(1/distance_i)
    p = sg.Point((0,0))
    def f(_x, i=None, verbose=0):
        energy = 0.0
        idxs = range(_x.shape[0]) if (i is None) else [i]
        for idx in idxs:
            v = graph.vs[idx]
            shape = v['_shape']
            # hwid, hhgt = (shape.width/2, shape.height/2)
            # ctrx, ctry = shape.centroid
            bbox = shape.bbox

            # distance from bounding box (left, right, bottom, top)
            #dxl = (_x[idx][0] - bbox[0])/shape.width
            #dxr = (bbox[2] - _x[idx][0])/shape.width
            #dyb = (_x[idx][1] - bbox[1])/shape.height
            #dyt = (bbox[3] - _x[idx][1])/shape.height
            dxl = (_x[idx][0] - bbox[0])/shape.width
            dyb = (_x[idx][1] - bbox[1])/shape.height
            dx = dxl * (1-dxl)
            dy = dyb * (1-dyb)

            #if dxl>0 and dxr>0 and dyb>0 and dyt>0:
            #    de = -(np.log(dxl) + np.log(dxr) + np.log(dyb) + np.log(dyt))
            if dx>0 and dy>0:
                de = -(np.log(dx) + np.log(dy))
            else:
                de = np.inf

            energy += de
        return energy

    def fg(_x, i, verbose=0):
        # grad = np.zeros(x.shape)
        grad = np.zeros(_x.shape[1])
        v = graph.vs[i]
        shape = v['_shape']
        bbox = shape.bbox

        # distance from bounding box (left, right, bottom, top)
        #dxl = (_x[i][0] - bbox[0])/shape.width
        #dxr = (bbox[2] - _x[i][0])/shape.width
        #dyb = (_x[i][1] - bbox[1])/shape.height
        #dyt = (bbox[3] - _x[i][1])/shape.height
        dxl = (_x[i][0] - bbox[0])
        dxr = (bbox[2] - _x[i][0])
        dyb = (_x[i][1] - bbox[1])
        dyt = (bbox[3] - _x[i][1])

        if dxl>0 and dxr>0:
            #grad[0] = (1/shape.width)*(1/dxr - 1/dxl)
            grad[0] = (1/dxr - 1/dxl)
        else:
            grad[0] = np.inf

        if dyb>0 and dyt>0:
            #grad[1] = (1/shape.height)*(1/dyt - 1/dyb)
            grad[1] = (1/dyt - 1/dyb)
        else:
            grad[1] = np.inf

        return grad

    def fg2(_x, i=None, verbose=0):
        return ngrad(f, _x, i)

    return (f, fg)

def energy_fr_factory(graph, hierarchy, K=1, **kwargs):
    # adjust k depending on something?
    shape = hierarchy.vs[0]['_shape']
    k = K * np.sqrt(shape.width*shape.height / len(graph.vs))
    masks = np.ones((len(graph.vs), len(graph.vs)), bool)
    np.fill_diagonal(masks, False)

    adj = np.array(graph.get_adjacency().data)

    def f(_x, i=None, verbose=0):
        energy = 0.0
        idxs = range(len(graph.vs)) if (i is None) else [i]

        for idx in idxs:
            # get pairwise distances
            dists = np.linalg.norm(_x[idx]-_x, axis=1)[masks[idx]]

            # fr
            # attract = \int d_e^2/k = d^3/(3k)
            # repulse = \int -k^2/d = -k^2 ln(d)
            energy += -k**2 * np.sum(np.log(dists))
            energy += np.sum(np.power(dists, 3) * adj[idx][masks[idx]]) / (3 * k)

        if i is None: energy /= 2.0
        return energy

    def fg(_x, i=None, verbose=0):
        devns = _x[i]-_x
        dists = np.linalg.norm(devns, axis=1)
        dists2 = np.power(dists, 2)

        # prevent divide by 0
        dists2[i] = 1.0
        
        grad = np.zeros(2)

        # attraction for linked nodes
        # (1/k) * \sum_j |x_i - x_j| * (x_i - x_j)
        grad[0] = 1 / k * np.sum(dists * devns[:,0] * adj[i])
        grad[1] = 1 / k * np.sum(dists * devns[:,1] * adj[i])

        # repulsion for all nodes
        # -k^2 * \sum_j (x_i - x_j) / |x_i - x_j|^2
        grad[0] += -k**2 * np.sum(devns[:,0] / dists2)
        grad[1] += -k**2 * np.sum(devns[:,1] / dists2)

        return grad

    def fg2(_x, i=None, verbose=0):
        return ngrad(f, _x, i)

    return (f, fg)

def energy_relaxed_fr_factory(graph, hierarchy, K=1, W=1, **kwargs):
    # adjust k depending on something?
    shape = hierarchy.vs[0]['_shape']
    k = K * np.sqrt(shape.width*shape.height / len(graph.vs))
    masks = np.ones((len(graph.vs), len(graph.vs)), bool)
    np.fill_diagonal(masks, False)

    hierarchy_gvs, slice = get_graph_hierarchy_mapping(graph)
    hdists = get_hierarchy_distances(hierarchy)

    ng = len(graph.vs)
    ks = np.full((ng, ng), k) + W*hdists[slice][:,slice]

    adj = np.array(graph.get_adjacency().data)

    def f(_x, i=None, verbose=0):
        energy = 0.0
        idxs = range(len(graph.vs)) if (i is None) else [i]

        for idx in idxs:
            # get pairwise distances
            dists = np.linalg.norm(_x[idx]-_x, axis=1)[masks[idx]]

            # fr
            # attract = \int d_e^2/k = d^3/(3k)
            # repulse = \int -k^2/d = -k^2 ln(d)
            #energy += -k**2 * np.sum(np.log(dists))
            energy += -np.sum(np.power(ks[idx][masks[idx]], 2) * np.log(dists))
            energy += np.sum(np.power(dists, 3) * adj[idx][masks[idx]] / (3 * ks[idx][masks[idx]]))

        if i is None: energy /= 2.0
        return energy

    def fg(_x, i=None, verbose=0):
        devns = _x[i]-_x
        dists = np.linalg.norm(devns, axis=1)
        dists2 = np.power(dists, 2)

        # prevent divide by 0
        dists2[i] = 1.0
        
        grad = np.zeros(2)

        # attraction for linked nodes
        # (1/k) * \sum_j |x_i - x_j| * (x_i - x_j)
        #grad[0] = 1 / k * np.sum(dists * devns[:,0] * adj[i])
        #grad[1] = 1 / k * np.sum(dists * devns[:,1] * adj[i])
        grad[0] = np.sum(dists * devns[:,0] * adj[i] / ks[i])
        grad[1] = np.sum(dists * devns[:,1] * adj[i] / ks[i])

        # repulsion for all nodes
        # -k^2 * \sum_j (x_i - x_j) / |x_i - x_j|^2
        #grad[0] += -np.sum(k**2 * devns[:,0] / dists2)
        #grad[1] += -np.sum(k**2 * devns[:,1] / dists2)
        grad[0] += -np.sum(np.power(ks[i], 2) * devns[:,0] / dists2)
        grad[1] += -np.sum(np.power(ks[i], 2) * devns[:,1] / dists2)

        return grad

    def fg2(_x, i=None, verbose=0):
        return ngrad(f, _x, i)

    return (f, fg)

def energy_kk_factory(graph, hierarchy, K=1., L0=1, **kwargs):
    """Builds an energy function for Kamada-Kawai spring layout.

    :param graph: igraph graph with v['_shape'] parameter
    :param K: spring strength constant (high K -> stiffer spring, less node distance variation)
    :param L: spring length constant (scaling parameter, high L -> further apart nodes)
    :return: energy function
    """

    # consistent with igraph, we set infinite distances to max finite distance
    sps = np.array(graph.shortest_paths())
    max_finite = max(sps[np.isfinite(sps)])
    sps[np.isinf(sps)] = 1.0*max_finite

    L = (L0/max_finite)
    ls = L * sps

    # avoid divide by 0
    # can probably just do this with masked array, but whatever
    np.fill_diagonal(sps, 1.0)
    ks = K / sps
    np.fill_diagonal(ks, 0.0)

    def f(_x, i=None, verbose=0):
        energy = 0.0
        idxs = range(len(graph.vs)) if (i is None) else [i]
        for idx in idxs:
            # get pairwise distances
            dists = np.linalg.norm(_x[idx]-_x, axis=1)

            # spring: spring constant * (distances - ideal graph distances)^2
            energy += np.sum(ks[idx] * np.power(dists - ls[idx], 2))
        if i is None: energy /= 2.0
        return energy

    def fg(_x, i, verbose=0):
        devns = _x[i]-_x
        dists = np.linalg.norm(devns, axis=1)

        # prevent divide by 0
        dists[i] = 1.0
        
        # \sum_j k_ij * ((x_i - x_j) - (l_ij * (x_i - x_j)) / (|x_i-x_j|))
        grad = np.zeros(2)
        grad[0] = 2*np.sum(ks[i] * (devns[:,0] - ls[i]*devns[:,0]/dists))
        grad[1] = 2*np.sum(ks[i] * (devns[:,1] - ls[i]*devns[:,1]/dists))
        return grad

    def fg2(_x, i=None, verbose=0):
        return ngrad(f, _x, i)

    return (f, fg)

def energy_relaxed_kk_factory(graph, hierarchy, K=1., L0=1.0, **kwargs):
    """Builds an energy function for Kamada-Kawai spring layout.

    :param graph: igraph graph with v['_shape'] parameter
    :param K: spring strength constant (high K -> stiffer spring, less node distance variation)
    :param L0: spring length constant (scaling parameter, high L -> further apart nodes)
    :return: energy function
    """

    # (1) set up spring lengths
    # consistent with igraph, we set infinite distances to max finite distance
    sps = np.array(graph.shortest_paths())
    max_finite = max(sps[np.isfinite(sps)])
    # if only self-loops exist, i.e. totally disconnected graph
    if max_finite == 0:
        max_finite = 1.0
    sps[np.isinf(sps)] = 1.0*max_finite

    L = (L0/max_finite)
    ls0 = L * sps

    # avoid divide by 0
    # can probably just do this with masked array, but whatever
    np.fill_diagonal(sps, 1.0)
    ks = K / sps
    np.fill_diagonal(ks, 0.0)

    # (2) set up hierarchy distances
    # main graph <-> hierarchy mapping
    hierarchy_gvs, slice = get_graph_hierarchy_mapping(graph)
    #hierarchy_gvs = defaultdict(list)
    #slice = []
    #for gv in graph.vs:
    #    hierarchy_gvs[gv['block']].append(gv.index)
    #    slice.append(gv['block'])

    hdists = get_hierarchy_distances(hierarchy)

    # expand to full size
    ls = ls0 + hdists[slice][:,slice]

    def f(_x, i=None, verbose=0):
        energy = 0.0
        idxs = range(len(graph.vs)) if (i is None) else [i]
        for idx in idxs:
            # get pairwise distances
            dists = np.linalg.norm(_x[idx]-_x, axis=1)

            # base: spring constant * (distances - ideal graph distances)^2
            # modified: spring constant * (distances - inter-block distances - ideal graph distances)^2
            energy += np.sum(ks[idx] * np.power(dists - ls[idx], 2))
            #energy += np.sum(ks[idx] * np.power(dists - ls[idx] - gdists[idx], 2))
        if i is None: energy /= 2.0
        return energy

    def fg(_x, i, verbose=0):
        devns = _x[i]-_x
        dists = np.linalg.norm(devns, axis=1)

        # prevent divide by 0
        dists[i] = 1.0
        
        # \sum_j k_ij * ((x_i - x_j) - (l_ij * (x_i - x_j)) / (|x_i-x_j|))
        grad = np.zeros(2)
        # grad[0] = 2*np.sum(ks[i] * (devns[:,0] - ls[i]*devns[:,0]/dists))
        # grad[1] = 2*np.sum(ks[i] * (devns[:,1] - ls[i]*devns[:,1]/dists))
        springs = ks[i] * (1 - ls[i] / dists)
        grad[0] = 2*np.sum(devns[:,0] * springs)
        grad[1] = 2*np.sum(devns[:,1] * springs)
        return grad

    def fg2(_x, i=None, verbose=0):
        return ngrad(f, _x, i)

    return (f, fg)



def energy_subgraph_kk_factory(graph, hierarchy, K=1., Ladjust=1.0, **kwargs):
    """Builds an energy function for Kamada-Kawai spring layout.

    :param graph: igraph graph with v['_shape'] parameter
    :param K: spring strength constant (high K -> stiffer spring, less node distance variation)
    :param L: spring length constant (scaling parameter, high L -> further apart nodes)
    :return: energy function
    """

    # consistent with igraph, we set infinite distances to max finite distance
    sps = np.array(graph.shortest_paths())
    max_finite = max(sps[np.isfinite(sps)])
    # if only self-loops exist, i.e. totally disconnected graph
    if max_finite == 0:
        max_finite = 1.0
    sps[np.isinf(sps)] = 1.0*max_finite

    # get ancestor sequences for hierarchy
    ancestors = get_ancestors(hierarchy)

    # main graph <-> hierarchy mapping
    hierarchy_gvs = defaultdict(list)
    slice = []
    for gv in graph.vs:
        hierarchy_gvs[gv['block']].append(gv.index)
        slice.append(gv['block'])

    n = len(hierarchy.vs)
    Ls = np.zeros((n,n))

    for i in range(len(hierarchy.vs)):
        ivs = hierarchy_gvs[i]
        for j in range(i, len(hierarchy.vs)):
            if i==j:
                jvs = ivs
                self_space = hierarchy.vs[i]['_self_space']
                if self_space is not None:
                    bbox = self_space.bbox
                else:
                    bbox = (0,0,0,0)
            else:
                jvs = hierarchy_gvs[j]
                lca = get_lca(i, j, ancestors)
                bbox = hierarchy.vs[lca]['_shape'].bbox

            # get graph diameter and available space
            l0 = np.sqrt((bbox[2]-bbox[0])*(bbox[3]-bbox[1]))
            sub_sps = sps[ivs][:,jvs]
            if np.prod(sub_sps.shape) > 0:
                max_dist = np.max(sub_sps)
            else:
                max_dist = 1.0
            max_dist = 1.0 if max_dist == 0 else max_dist

            # unit length
            L = l0 / max_dist
            Ls[i,j] = L
            Ls[j,i] = L

    # expand to full size
    ls = Ls[slice][:,slice]
    ls = Ladjust * ls * sps

    # avoid divide by 0
    # can probably just do this with masked array, but whatever
    np.fill_diagonal(sps, 1.0)
    ks = K / sps
    np.fill_diagonal(ks, 0.0)

    def f(_x, i=None, verbose=0):
        energy = 0.0
        idxs = range(len(graph.vs)) if (i is None) else [i]
        for idx in idxs:
            # get pairwise distances
            dists = np.linalg.norm(_x[idx]-_x, axis=1)

            # spring: spring constant * (distances - ideal graph distances)^2
            energy += np.sum(ks[idx] * np.power(dists - ls[idx], 2))
        if i is None: energy /= 2.0
        return energy

    def fg(_x, i, verbose=0):
        devns = _x[i]-_x
        dists = np.linalg.norm(devns, axis=1)

        # prevent divide by 0
        dists[i] = 1.0
        
        # \sum_j k_ij * ((x_i - x_j) - (l_ij * (x_i - x_j)) / (|x_i-x_j|))
        grad = np.zeros(2)
        # grad[0] = 2*np.sum(ks[i] * (devns[:,0] - ls[i]*devns[:,0]/dists))
        # grad[1] = 2*np.sum(ks[i] * (devns[:,1] - ls[i]*devns[:,1]/dists))
        springs = ks[i] * (1 - ls[i] / dists)
        grad[0] = 2*np.sum(devns[:,0] * springs)
        grad[1] = 2*np.sum(devns[:,1] * springs)
        return grad

    def fg2(_x, i=None, verbose=0):
        return ngrad(f, _x, i)

    # return (f, fg, ls)
    return (f, fg)


def energy_label_factory(graph, hierarchy, textsize=0.16, **kwargs):
    """Constructs an energy function for label placement.

    The general idea is as follows: we create an energy function such that energy is high
    when nodes are on roughly the same y-coordinate and close by on the x-axis,
    where 'close by' is determined by the combined width of their labels.

    In practice, the energy function is a gaussian distribution where the y-axis sigma (SD) is constant,
    and x-axis sigma equals the combined label widths, so that very wide nodes get pulled close together.
    Both sigmas are also scaled by the size of the text.

    :param graph: igraph graph
    :param textsize: how many units of the display does a letter take up
        e.g. if each unit of plot corresponds to an inch, 12pt (=1/6in) text takes up about 0.16 units
    :return: the appropriate energy function
    """
    widths = [len(v['name']) for v in graph.vs]
    widthmat = textsize*np.array([[[w1+w2,1] for w2 in widths] for w1 in widths])

    def f(_x, i=None):
        energy = 0.0
        idxs = range(len(graph.vs)) if (i is None) else [i]
        for idx in idxs:
            expargs = np.power((_x[idx]-_x)/widthmat[idx], 2)
            exparg = np.sum(expargs, axis=1)
            # exps = np.exp(-exparg)
            # energy += np.sum(exps)
            energy += np.sum(np.exp(-exparg))
        return energy

    def fg(_x, i, verbose=0):
        zs = (_x[i]-_x)/widthmat[i]
        grad_devs = zs/widthmat[i]

        expargs = np.power(zs, 2)
        exparg = np.sum(expargs, axis=1)
        exps = np.exp(-exparg)

        grad = np.zeros(2)
        grad[0] = -2 * np.sum(exps * grad_devs[:,0])
        grad[1] = -2 * np.sum(exps * grad_devs[:,1])

        return grad

    def fg2(_x, i, verbose=0):
        return ngrad(f, _x, i=i)

    return (f, fg)

def energy_factory(graph, hier,
        energy_dict = {'kk': dict(), 'shape': dict(), 'label': dict()}):
    """Combines shape and Kamada-Kawai energy functions.
    Because KK and shape energy functions are in general on unrelated scales,
    relative weighting between them is key and varies across graphs.
    I can think of no great way to determine the appropriate ratio automatically.
    Consider: measure ratio in initial burn-in runs?

    :param graph: igraph graph
    :param weights: weight dictionary ('kk': kamada kawai weight, 'shape': shape weight)
    :param kkargs: arguments to pass to kamada kawai energy function
    :return: combined energy function
    """
    
    # initialize energy functions
    fs = dict()
    for (k, d) in energy_dict.items():
        if '_wgt' not in d:
            wgt = 1.0
        elif d['_wgt'] == 0.0:
            continue
        else:
            wgt = d['_wgt']

        (f, fg) = _ENERGY[k](graph, hier, **d)
        fs[k] = { 'wgt': wgt, 'f': f, 'fg': fg }

    def f(_x, i=None, verbose=0):
        energy = 0.0
        for k, fd in fs.items():
            energy += fd['wgt'] * fd['f'](_x, i)
        return energy

    def fg(_x, i=None, verbose=0):
        grad = np.zeros(_x.shape[1])
        for k, fd in fs.items():
            grad += fd['wgt'] * fd['fg'](_x, i)
        return grad

    return (f, fg)

_ENERGY = {
        'fr': energy_fr_factory,
        'relaxed_fr': energy_relaxed_fr_factory,
        'kk': energy_kk_factory,
        'relaxed_kk': energy_relaxed_kk_factory,
        'subgraph_kk': energy_subgraph_kk_factory,
        'label': energy_label_factory,
        'shape': energy_shape_factory 
    }

############################################################
# default energy dict

def get_energy_dict(g,
        energy_dict=None, energy_dict_include=['subgraph_kk', 'shape', 'label'],
        **kwargs):
    _energy_dict = dict()

    if 'kk' in energy_dict_include:
        _energy_dict['kk'] = { '_wgt': 1.0e0, 'K': 1e0, 'L0': 1e0 }

    if 'relaxed_kk' in energy_dict_include:
        _energy_dict['relaxed_kk'] = { '_wgt': 1.0e0, 'K': 1e0, 'L0': 1e0 }

    if 'subgraph_kk' in energy_dict_include:
        _energy_dict['subgraph_kk'] = { '_wgt': 1.0e0, 'K': 1e0, 'Ladjust': 7.1e-1 }

    if 'fr' in energy_dict_include:
        _energy_dict['fr'] = { '_wgt': 1.0e0, 'K': 1e0 }

    if 'relaxed_fr' in energy_dict_include:
        _energy_dict['relaxed_fr'] = { '_wgt': 1.0e0, 'K': 1e0, 'W': 1e0 }

    if 'shape' in energy_dict_include:
        _energy_dict['shape'] = { '_wgt': 2.0e1 }

    if 'label' in energy_dict_include and 'name' in g.vertex_attributes():
        _energy_dict['label'] = { '_wgt': 1.0e2, 'textsize': 1e-1 }

    # update dictionary
    if energy_dict is not None:
        for k, v in energy_dict.items():
            _energy_dict[k].update(v)

    # _energy_dict = {
    #         'subgraph_kk':  { '_wgt': 1.0e0, 'K': 1e0, 'Ladjust': 7.1e-1 },
    #         'kk':           { '_wgt': 0.0e0, 'K': 1e0, 'L0': np.sqrt(0.5*(bbox[2]-bbox[0])*(bbox[3]-bbox[1])) },
    #         'shape':        { '_wgt': 2.0e1 },
    #         'label':        { '_wgt': 1.0e2, 'textsize': 1e-1 }
    #     }

    return _energy_dict

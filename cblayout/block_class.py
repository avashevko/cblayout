
class Block():
    """
    Represents an individual block for layout.

    :param idx: index of this block
    :param parent: Parent of this block
    :param vertices: List of vertices in this block
    :param numelem: number of nodes in this block
    :param klevel: embeddedness of the block
    :param kconn: connectivity (cohesion) of the block
    """
    #TODO: klevel and kconn need to be changed
    def __init__(self, idx=None, parent=None, klevel=0, vertices=None, numelem=0, kconn=0):
        self.idx = idx
        self.parent = parent
        self.klevel = klevel
        if vertices == None:
            self.vertices = list()
        else:
            self.vertices = vertices
        self.numelem= numelem
        self.kconn = kconn


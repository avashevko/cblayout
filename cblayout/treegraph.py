# plumbing
import numpy as np
import igraph as ig
import networkx as nx
from collections import (OrderedDict, Counter)

# shapes
import shapely.geometry as sg
from shapely.ops import triangulate
from shapely import affinity

# treemap
from squarify import (squarify, normalize_sizes)

# for runner
from pickle import dump, load
from warnings import warn
import traceback

from . plotting import plot
from . energy import (energy_factory, get_energy_dict)
from . hierarchy_tools import (assign_property_to_hierarchy,
    assign_labels_to_hierarchy,
    bfs_tree,
    dfs_tree,
    hierarchy_size_walk,
    hierarchy_size_walk_iter,
    hierarchy_shapes,
    get_initial_claims,
    get_lca,
    get_subtree_claims)
from . autolegend import make_legend_patches_from_attribute

############################################################
# Node Shape

class NodeShape:
    def __init__(self, shape):
        self.parent = None
        self.children = []
        self.geom = shape
        if shape is not None:
            self._update_bounds()

    def add_child(self, child):
        self.children.append(child)
        child.parent = self

        if child.geom is not None:
            self.geom = self.geom.difference(sg.Polygon(child.geom.exterior))
            self._update_bounds()

    def _update_bounds(self):
        self.bounds = list(self.geom.interiors)
        self.bounds.append(self.geom.exterior)
        self.bbox = get_bounding_box(self.geom)
        self.width, self.height = (self.bbox[2]-self.bbox[0], self.bbox[3]-self.bbox[1])
        self.centroid = self.geom.centroid.coords[0]

def sample_shape(shape):
    """Draw a uniform random point from inside an arbitrary shape.

    :param shape: shapely Shape (not NodeShape)
    :return: random point within shape
    """
    ts = [t for t in triangulate(shape) if shape.contains(t)]
    cum_areas = np.cumsum(np.array([t.area for t in ts]))
    idx = np.sum(np.random.random_sample() * max(cum_areas) > cum_areas)
    p = sample_triangle(ts[idx])
    return p

def sample_triangle(t):
    """Draw a uniform random point from inside of a triangle.

    :param t: shapely triangle
    :return: random point
    """
    r1 = np.sqrt(np.random.random_sample(1)[0])
    r2 = np.random.random_sample(1)[0]
    sampler = np.array([[1-r1, 1-r1], [r1*(1-r2), r1*(1-r2)], [r2*r1, r2*r1]])
    return np.sum(np.array(t.exterior.coords)[0:3,:] * sampler, axis=0)

def random_layout_with_shapes(graph):
    """Taking an igraph graph in which each point is assigned a parent shape,
    returns a random layout of points within shapes.

    :param graph: igraph graph in which v['_shape'] is the point's parent shape
    :return: numpy array of positions
    """
    ps = np.array([sample_shape(v['_shape'].geom) for v in graph.vs])
    return ps

def get_bounding_box(shape):
    """
    Extracts rectangular bounding box from Shapely shape.

    :param shape: Shapely shape object
    :return: tuple (bottom left x, bottom left y, top right x, top right y)
    """
    evlp = shape.envelope.exterior.coords
    maxx = max(evlp[0][0], evlp[1][0], evlp[2][0])
    minx = min(evlp[0][0], evlp[1][0], evlp[2][0])
    maxy = max(evlp[0][1], evlp[1][1], evlp[2][1])
    miny = min(evlp[0][1], evlp[1][1], evlp[2][1])
    return (minx, miny, maxx, maxy)

def clip_corners(shape):
    """Clips the corners of a rectangle, such that only 1/3 of short side remains,
    and leaves 45 degree corners.

    :param shape: Shapely shape object
    """
    (ctrx, ctry) = shape.centroid.coords[0]

    # get dimensions
    bbox = get_bounding_box(shape)
    wid = bbox[2] - bbox[0]
    hgt = bbox[3] - bbox[1]

    long = max(wid, hgt)
    short = min(wid, hgt)

    # half side of the desired clipping square
    hside = np.sqrt(2)*((long/2) + (short/6)) / 2.
    clipbox = affinity.rotate(sg.box(ctrx-hside, ctry-hside, ctrx+hside, ctry+hside), 45)
    return shape.intersection(clipbox)

############################################################
# SGD

def sgd(f, fg, x, max_iter=1e2, ftol=1e-8, xtol=1e-8, gradtol=1e-6, verbose=0, alpha0=1e-1, tau=0.1, **kwargs):
    """
    Implements a sort of stochastic gradient descent.

    Within each iteration, randomly permutes observations and calculates gradient for each one.
    Current stopping condition is maximum iterations only.

    Notes on stopping conditions:
    This algorithm relies on a dynamic step size in the direction of the gradient.
    This step size tends to fall over time as the algorithm prefers smaller jumps near the optimum.
    Unfortunately, this also makes the expected change in f and change in x smaller over time.
    Stopping criteria reliant on the magnitude of f or x will be mechanically more likely to fire over time,
    as a side effect of this declining step size. This can be problematic.

    :param f: function to minimize
    :param x: initial position matrix (numpy array)
    :param max_iter: maximum number of iterations to run
    :param ftol: maximum change in f between iterations to stop the algorithm
    :param xtol: maximum change in x (euclidean norm) between iterations to stop the algorithm
    :param gradtol: maximum size of gradient to stop the algorithm
    :param verbose: verbosity; primarily to display iteration progress
    :param alpha0: initial step size for gradient descent step
    :param tau: step size reduction parameter (i.e. alpha = tau*alpha)
    :return: x, f, and gradient at minimum, along with diagnostic info
    """
    _iter = 0
    x0 = x.copy()
    f0 = f(x0)
    fcalls = 1
    backtraces = 0
    alpha = alpha0

    fdist = xdist = gradnorm = np.inf
    order = list(range(x.shape[0]))
    while _iter < max_iter and gradnorm > gradtol:
        np.random.shuffle(order)
        grad = np.zeros(x0.shape)
        x00 = x0
        iter_backtraces = 0
        iter_fcalls = 0
        alphas = []
        for i in order:
            sgd_res = sgd_step(f, fg, x0, i, alpha0=alpha, tau=tau, verbose=verbose, **kwargs)
            iter_backtraces += sgd_res['backtraces']
            iter_fcalls += sgd_res['fcalls']
            alphas.append(sgd_res['alpha'])

            # update points during sgd
            x0 = sgd_res['x']
            grad[i] += sgd_res['grad']
        backtraces += iter_backtraces
        fcalls += iter_fcalls

        # hysteretic theta 2
        # start above highest alpha from previous round
        alpha = max(alphas)/tau
        if verbose>2:
            print('alpha to {}'.format(alpha))

        x1 = x0
        f1 = f(x1)
        fcalls += 1

        # loop parameters
        # n.b. compare x1 to x00, since x0 changes during sgd steps
        fdist = abs(f1-f0)
        xdist = np.linalg.norm(x1-x00)
        gradnorm = np.linalg.norm(grad)
        # x0 = x1
        f0 = f1
        _iter += 1

        if verbose > 0:
            # step = 10 if verbose == 1 else 1
            step = 10
            if _iter % step == 0:
                print('SGD iter {:3}, f: {:10.5f}, |df|: {:10.5f}, bts: {:5} ({:0.2f}%), iter bt: {:4} ({:0.2f}%)'.format(
                    _iter, f0, gradnorm,
                    backtraces, (100*backtraces/fcalls),
                    iter_backtraces, (100*iter_backtraces/iter_fcalls)))
                f(x1, verbose=1)

    termination = []
    if _iter >= max_iter: termination.append('max iterations')
    if fdist < ftol: termination.append('|f1-f0| below tolerance')
    if xdist < xtol: termination.append('|x1-x0| below tolerance')
    if gradnorm < gradtol: termination.append('|grad| below tolerance')

    return { 'x': x0, 'f': f0, 'grad': grad,
             'termination': termination,
             'iter': _iter, 'fdist': fdist, 'xdist': xdist, 'gradnorm': gradnorm,
             'backtraces': backtraces, 'fcalls': fcalls }

def sgd_step(f, fg, x, i, alpha0=1e-2, eps=1e-8, tau=0.5, crit=0.5, verbose=0, **kwargs):
    """
    Stochastic gradient descent step. Calculates a numerical gradient, then an acceptable step size.

    Step size calculation is a backtracking line search.

    :param f: function to minimize
    :param x: position matrix
    :param i: position id (row)
    :param alpha0: initial step size for gradient descent
    :param eps: epsilon for gradient calculations (i.e. how far to shift position)
    :param tau: alpha-reduction coefficient for backtracing line search (expect (0,1))
    :param crit: gradient expectation for backtracing line search (expect (0,1))
    :return: x, f, and gradient, along with diagnostic info
    """

    grad = fg(x, i, verbose)

    f0 = f(x, i, verbose)
    fcalls = 1

    # armijo-goldstein backtracing line search
    # search direction
    p = -grad / np.linalg.norm(grad)
    p = np.nan_to_num(p)

    # expected delta f from shift in unit direction
    m = np.sum(p * grad)

    alpha = alpha0
    xa = x.copy()
    xa[i] = x[i] + alpha*p
    fa = f(xa, i, verbose)
    fcalls += 1
    lsi = 0

    # while change from f0 to candidate fa is smaller than expected change from gradient (alpha*m)
    # but don't go near gradient precision
    while (f0 - fa) < (-crit * alpha * m) and (alpha>tau*tau*eps):
        alpha = tau * alpha
        xa[i] = x[i] + alpha*p
        fa = f(xa, i, verbose)
        fcalls += 1
        lsi += 1

    return {'x': xa, 'f': fa, 'grad': grad, 'backtraces': lsi, 'fcalls': fcalls, 'alpha': alpha}

############################################################
# Graph conversion functions

def layout_from_np(arr):
    """Converts numpy array to igraph layout."""
    return ig.Layout(arr.tolist())

def networkx_pos_from_np(arr):
    """Converts numpy array to networkx position dictionary.
    
    Assumes that node ids are sequential integers.
    """
    d = dict()
    for i in range(arr.shape[0]):
        d[i] = arr[i]
    return d

def igraph_to_networkx(g):
    """Light conversion of igraph to networkx graph."""
    nxg = nx.Graph()
    for v in g.vs:
        nxg.add_node(v.index, **v.attributes())
    for e in g.es:
        nxg.add_edge(e.source, e.target, **e.attributes())
    return nxg

def calculate_block_size(hier, G):
    """Calculate hierarchy size by summing up the number of nodes in a particular block.
    
    :param hier: iGraph hierarchy graph, where each vertex is a block
    :param G: the underlying iGraph Graph object
    
    .. note:
        Has the side effect of generating a self_size attribute for each hierarchy block.
    """
    # set self-size is 0 for all vertices
    hier.vs['self_size'] = 0
    # then for each vertice we increase the count of the block it belongs
    for gv in G.vs:
        hier.vs[gv['block']]['self_size'] += 1

############################################################
# Tree traversal

def hierarchy_try_bbox_iter(hier, bbox, dfs_traversal, claims,
        label_buffer=0.1, pad=0.05, pad_percent=True, **kwargs):
    stclaims = get_subtree_claims(claims, dfs_traversal)
    
    # Generate a reverse traversal, since we will first start from the root
    rev_traversal = dfs_traversal[::-1]
    # assign a bounding box to root
    rev_traversal[0]['bbox'] = bbox

    # global statistics
    graph_wid, graph_hgt = (bbox[2]-bbox[0], bbox[3]-bbox[1])
    graph_area = graph_wid*graph_hgt
    graph_size = sum(hier.vs[0]['sizes'].values())
    stats = OrderedDict()

    # top-down DFS
    for v in rev_traversal:
        # get the bounding box
        bbox = v['bbox']
        if bbox is not None:
            wid, hgt = (bbox[2] - bbox[0], bbox[3] - bbox[1])

            # for padding the rect
            if pad_percent:
                padx = pad*wid
                pady = pad*hgt
            else:
                padx = pady = pad

            # assemble self and child sizes
            claim_dict = OrderedDict([(v['_idx'], claims[v['_idx']])])
            for c in sorted(v['children'], key=lambda c: stclaims[c['_idx']], reverse=True):
                idx = c['_idx']
                claim_dict[idx] = stclaims[idx]

            # # check for empty nodes
            # positive = OrderedDict([(k, v>0) for (k,v) in claim_dict.items()])
            # if not all(positive.values()):
            #     print(claim_dict)
            
            # make treemap from non-empty nodes
            # size ordering is not necessary at all
            # see the paper at https://www.win.tue.nl/~vanwijk/stm.pdf
            [positives, areas] = zip(*[(k,v) for (k,v) in claim_dict.items() if v>0])
            if len(positives) > 0:
                squares = squarify(normalize_sizes(areas, wid-2*padx, hgt-2*pady-label_buffer),
                               bbox[0]+padx, bbox[1]+pady+label_buffer,
                               bbox[2]-bbox[0]-2*padx,
                               bbox[3]-bbox[1]-2*pady-label_buffer)
            else:
                squares = []
            assorted_squares = dict(zip(positives, squares))
        else:
            assorted_squares = dict()

        # self space
        if v['_idx'] in assorted_squares:
            sq = assorted_squares[v['_idx']]
            ownbox = (
                    sq['x'] + padx,
                    sq['y'] + pady,
                    sq['x'] + sq['dx'] - padx,
                    sq['y'] + sq['dy'] - pady
                )
        else:
            ownbox = None
        v['self_bbox'] = ownbox

        # compile statistics
        subtree_area = 0 if bbox is None else (wid - 2*padx)*(hgt - 2*pady)
        node_area = 0 if ownbox is None else (ownbox[2]-ownbox[0])*(ownbox[3]-ownbox[1])
        vstats = treemap_statistics(v, graph_size, graph_area,
                subtree_area, node_area)
        stats[v['_idx']] = vstats

        # Assign a bounding box to each child
        # by using assorted_squares dict
        for child in v['children']:
            idx = child['_idx']
            if idx in assorted_squares:
                sq = assorted_squares[idx]
                cbbox = (sq['x'] + padx,
                        sq['y'] + pady,
                        sq['x'] + sq['dx'] - padx,
                        sq['y'] + sq['dy'] - pady)
                child['bbox'] = cbbox
            else:
                child['bbox'] = None
    
    return stats

def treemap_statistics(v, graph_size, graph_area, subtree_area, node_area):
    vstats = dict()

    sizes = v['sizes']
    vstats['label'] = ('.' * (v['_klevel']-1)) + str(v['_idx'])
    # basic stats
    subtree_size = sum(sizes.values())
    vstats['total_size'] = subtree_size
    vstats['total_space'] = subtree_area
    # space per node
    vstats['total_space_per_node'] = subtree_area / subtree_size if subtree_size > 0 else None
    # self stats
    self_size = sizes[v['_idx']]
    vstats['self_size'] = self_size
    if self_size > 0:
        vstats['self_space'] = node_area
        vstats['self_space_per_node'] = node_area / self_size
    else:
        vstats['self_space'] = 0
        vstats['self_space_per_node'] = None
    return vstats

def hierarchy_apply_bbox_iter(hier, dfs_traversal):
    # Generate a reverse traversal, since we will first start from the root
    rev_traversal = dfs_traversal[::-1]
    aesthetic_flip = None

    # give each shape its overall and internal bounding boxes
    # TODO: check for lack of internal bounding box?
    for v in rev_traversal:
        bbox = v['bbox']
        ownbox = v['self_bbox']

        # flip axes based on root node: make root node horizontal
        if aesthetic_flip is None:
            aesthetic_flip = (ownbox is not None) and (ownbox[3]-ownbox[1] > ownbox[2]-ownbox[0])

        if aesthetic_flip:
            bbox = (bbox[1], bbox[0], bbox[3], bbox[2])
            if ownbox is not None:
                ownbox = (ownbox[1], ownbox[0], ownbox[3], ownbox[2])

        if bbox is not None:
            shape = NodeShape(sg.box(bbox[0], bbox[1], bbox[2], bbox[3]))
        else:
            shape = NodeShape(None)
        v['_shape'] = shape

        if ownbox is not None:
            self_space = NodeShape(sg.box(ownbox[0], ownbox[1], ownbox[2], ownbox[3]))
            v['_self_space'] = self_space
        else:
            v['_self_space'] = None

    # now start from the leaves and go up
    # add the child shape to the parent shape
    for v in dfs_traversal:
        for child in v['children']:
            v['_shape'].add_child(child['_shape'])

def improve_claims(claims, stats, key='self_space_per_node'):
    claims1 = dict()
    nodespaces = [vs[key] for vs in stats.values() if vs[key] is not None and vs[key]>0]
    max_nodespace = max(nodespaces)
    gaps = [max_nodespace / ns for ns in nodespaces]
    max_gap = max(gaps)

    for (idx, claim) in claims.items():
        idx_nodespace = stats[idx]['self_space_per_node']
        if idx_nodespace is None:
            claims1[idx] = claim
        elif idx_nodespace == 0:
            claims1[idx] = max_gap**2 * claim
        else:
            claims1[idx] = claim * (max_nodespace / idx_nodespace)

    return claims1

def space_gap(stats, key='self_space_per_node'):
    # nodespaces = [vs[key] for vs in stats.values() if vs[key] is not None]
    nodespaces = [vs[key] for vs in stats.values() if vs[key] is not None and vs[key]>0]
    max_nodespace = max(nodespaces)
    gaps = [max_nodespace / ns for ns in nodespaces]
    return max(gaps)

def print_size_stats(stats, buf=2):
    order = [
            ('id', 'label', '-%ds'),
            ('tsi', 'total_size', '%dd'),
            ('tsp', 'total_space', '%d.3f'),
            ('tspn', 'total_space_per_node', '%d.3f'),
            ('ssi', 'self_size', '%dd'),
            ('ssp', 'self_space', '%d.3f'),
            ('sspn', 'self_space_per_node', '%d.3f')
        ]
    headings, ks, formats = zip(*order)

    # get max column widths
    maxes = dict([(i, len(headings[i])) for i in range(len(order))])
    for (k,vs) in stats.items():
        for i in range(len(order)):
            if vs[ks[i]] is not None:
                ilen = len(('%' + formats[i] % 0) % (vs[ks[i]]))
            else:
                ilen = 1
            # if i not in maxes: maxes[i] = ilen
            maxes[i] = max(maxes[i], ilen)
    rformats = ['%' + (formats[i] % (maxes[i] + buf)) for i in range(len(order))]
    hformat = ''.join(['%' + ('%ds' % (maxes[i] + buf)) for i in range(len(order))])

    # print table header and rows
    print(hformat % tuple(headings))
    for (k,vs) in stats.items():
        for i in range(len(order)):
            if vs[ks[i]] is not None:
                print(rformats[i] % vs[ks[i]], end='')
            else:
                print(('%%%ds' % (maxes[i] + buf)) % '-', end='')
        print()

def hierarchy_iterate_treemap(hier, bbox, dfs_traversal, verbose=0, **kwargs):
    # generate initial claims
    claims = get_initial_claims(hier)
    # check for empty subtrees
    stclaims = get_subtree_claims(claims, dfs_traversal)
    if any([v==0 for v in stclaims.values()]):
        if verbose>1:
            print('Size claims by block:\n%s\n' % stclaims)
            # print(stclaims)
            # print()

    # improve treemap through iteration
    # claim improvement algorithm is probably a fixed point algorithm
    # unless small claim adjustment -> radically different squarify solution
    # which seems incredibly possible, if rare
    tmiter = 0
    if verbose>1:
        print('Optimizing block size claims:')
    # do until:
    while True:
        stats = hierarchy_try_bbox_iter(hier, bbox, dfs_traversal, claims, **kwargs)
        self_gap = space_gap(stats) - 1
        total_gap = space_gap(stats, 'total_space_per_node') - 1
        tmiter += 1
        if verbose>1:
            print('max gap %2d: self %0.2e total %0.1e' % (tmiter, self_gap, total_gap))
        if verbose>2:
            print_size_stats(stats)
        # do until:
        if tmiter >= 10 or self_gap < (1e-8):
            break
        else:
            claims = improve_claims(claims, stats)
    if verbose==2:
        print_size_stats(stats)


def block_layout(graph_and_blk_tree,
    bbox=(-10,-10,10,10),
    pad=0.02,
    pad_percent=False, 
    label_buffer=0,
    **kwargs):

    # extract and copy
    g, blks, hier = (graph_and_blk_tree['g'], graph_and_blk_tree['blks'], graph_and_blk_tree['hier'])
    g = g.copy()
    hier = hier.copy()

    # get energy dictionary
    energy_dict = get_energy_dict(g, **kwargs)

    # assign block properties to hierarchy
    assign_property_to_hierarchy(hier, blks)
    assign_labels_to_hierarchy(hier)
    # calculate hierarchy size
    calculate_block_size(hier, g)
    # generate the traversal orders
    parent, bfs_traversal = bfs_tree(hier, 0)
    dfs_traversal = dfs_tree(hier, 0)
    # calculate children sizes and find bounding boxes
    hierarchy_size_walk_iter(hier, parent, dfs_traversal)
    # calculate treemap bounding boxes
    hierarchy_iterate_treemap(hier, bbox, dfs_traversal,
            pad=pad, pad_percent=pad_percent, label_buffer=label_buffer,
            **kwargs)
    # apply bounding boxes
    hierarchy_apply_bbox_iter(hier, dfs_traversal)

    # assign shape to original vertices
    for v in g.vs:
        v['_shape'] = hier.vs[v['block']]['_self_space']

    # initial random layout
    ps0 = random_layout_with_shapes(g)

    # build energy function
    f, fg = energy_factory(g, hier, energy_dict)

    # calculate positions
    ps1 = sgd(f, fg, ps0, **kwargs)

    # make networkx graph for plotting
    nxg = igraph_to_networkx(g)
    pos = networkx_pos_from_np(ps1['x'])

    return (nxg, hier, pos)


def load_block_set(graph_and_blk_tree_set_file,
    group=None,
    subgroup_from=None,
    subgroup_to=None,
    group_key=None,
    subgroup_key=None,
    graph_and_blk_tree_map_file_key=None,
    graph_and_blk_tree_key=None):
    """Load a set of graphs and their associated block trees
    from a file, and roll them into the graph and block tree set.

    This function has a general interface so that we can load
    graphs grouped under a category (say region) and further
    divided into time periods.
    Each item in the set is expected to be a dict object.
    We expect each item to point to a file url where
    where the graph and block tree objects for a particular
    graph are stored, e.g., graph and block tree for China
    between 1992 and 1995.

    :param graph_and_blk_tree_set_file: persistent storage file path for the graph and block tree objects
    :param group: restrict set to group
    :param subgroup_from: restrict set to a subgroup starting at `from`, inclusive
    :param subgroup_to: restrict set to a subgroup ending at `to`, inclusive
    :param group_key: group selector in the dict object, e.g., `region`
    :param subgroup_key: subgroup selector in the dict object, e.g., `interval`
    :param graph_and_blk_tree_map_file_key: graph and block tree file url selector
    :param graph_and_blk_tree_key: graph and block tree object selector
    :return: list of block tree descriptors and files
    """
    with open(graph_and_blk_tree_set_file, 'rb') as fin:
        graph_and_blk_tree_set = load(fin)

    # subset
    if group is not None:
        graph_and_blk_tree_set = [x for x in graph_and_blk_tree_set if x[group_key] == group]
    if subgroup_from is not None:
        graph_and_blk_tree_set = [x for x in graph_and_blk_tree_set if x[subgroup_key].start >= subgroup_from]
    if subgroup_to is not None:
        graph_and_blk_tree_set = [x for x in graph_and_blk_tree_set if x[subgroup_key].end <= subgroup_to]

    # sort ascending by year
    graph_and_blk_tree_set.sort(key = lambda x: x[subgroup_key].start)

    # load graph_and_blk_tree files and roll into set
    for graph_and_blk_tree_map in graph_and_blk_tree_set:
        graph_and_blk_tree_file = graph_and_blk_tree_map[graph_and_blk_tree_map_file_key]
        with open(graph_and_blk_tree_file, 'rb') as fin:
            graph_and_blk_tree = load(fin)
            graph_and_blk_tree_map[graph_and_blk_tree_key] = graph_and_blk_tree
    return graph_and_blk_tree_set


def block_plot(graph_and_blk_tree=None,
    block_file=None,
    replot_prefix=None,
    bbox=(-10, -10, 10, 10),
    margin=1,
    label_buffer=0,
    shape_color_map=None,
    legend_map_list=None,
    legend_font_size=None,
    legend_ncol=None,
    legend_loc='lower center',
    edge_style_map=None,
    edge_style_key=None,
    edge_groups=None,
    **kwargs):
    """Plot individual graph and associated block tree.

    :param block_file: persistent storage url where graph and block tree objects are stored
    :param replot_prefix: save data for replotting
    :param bbox:
    :param margin:
    :param label_buffer:
    :param shape_color_map: dict of {shape:color}
    :param legend_map_list: a list of legend attribute dicts
    :param legend_font_size: legend font size, if different from node font size
    :param legend_ncol: num of columns in legend
    :param legend_loc:
    :param edge_style_map: a dict of dicts for styling edges
    :param edge_style_key: selector for edge_style_map
    :param edge_groups: group of edges based on edge_style_key
    :param *kwargs: kwargs to be passed to drawing functions
    """
    # check inputs
    if block_file is None and graph_and_blk_tree is None:
        raise ValueError("Attention, s'il vous plait: Must provide either graph_and_blk_tree or block_file argument")
    elif block_file is not None and graph_and_blk_tree is not None:
        warn('Awas: Both graph_and_blk_tree and block_file provided; using graph_and_blk_tree.')
    
    if graph_and_blk_tree is None:
        with open(block_file, 'rb') as fin:
            graph_and_blk_tree = load(fin)

    g, blks, hier = (graph_and_blk_tree['g'],
                    graph_and_blk_tree['blks'],
                    graph_and_blk_tree['hier'])

    nxg, hier, pos = block_layout(graph_and_blk_tree,
                                bbox=bbox,
                                label_buffer=label_buffer,
                                **kwargs)

    # make labels
    if 'labels' not in kwargs:
        labels = dict()
        for i in nxg.nodes():
            if 'name' in g.vertex_attributes():
                labels[i] = g.vs[i]['name']
            else:
                labels[i] = i
        kwargs['labels'] = labels

    # make legends
    if legend_map_list is not None:
        # legend_map_list contains dicts of legend attributes
        # iterate through that list of dicts, add a patch for
        # each element
        legends = []
        for legend_map in legend_map_list:
            _patches = make_legend_patches_from_attribute(g,
                _type=legend_map['type'],
                values=legend_map['values'],
                colors=legend_map['colors'],
                shapes=legend_map['shapes'],
                linewidth=legend_map['linewidth'],
                fontsize=legend_font_size,
                markersize=legend_map['markersize'])
            # patches is a list
            # ax.legend expects a list for the handles arg
            for _patch in _patches:
                legends.append(_patch)
    else:
        legends = None

    # save relevant data for replotting
    if replot_prefix is not None:
        with open('%s-networkx.pickle' % replot_prefix, 'wb') as f:
            dump(nxg, f)
        with open('%s-positions.pickle' % replot_prefix, 'wb') as f:
            dump(pos, f)
        with open('%s-hierarchy.pickle' % replot_prefix, 'wb') as f:
            dump(hier, f)

    # make plot
    plot(nxg,
        hier,
        pos,
        xlim=[bbox[0] - margin, bbox[2] + margin],
        ylim=[bbox[1] - margin, bbox[3] + margin],
        legends=legends,
        label_buffer=label_buffer,
        shape_color_map=shape_color_map,
        legend_font_size=legend_font_size,
        legend_ncol=legend_ncol,
        legend_loc=legend_loc,
        edge_groups=edge_groups,
        edge_style_key=edge_style_key,
        edge_style_map=edge_style_map,
        **kwargs)


def plot_block_set(graph_and_blk_tree_set=None,
    graph_and_blk_tree_set_file=None,
    prefix=None,
    subtitle=None,
    preprocessors=[],
                   bbox=(-10, -10, 10, 10),
    verbose=0,
    title=None,
    group=None,
    subgroup_from=None,
    subgroup_to=None,
    group_key='region',
    subgroup_key='interval',
    graph_and_blk_tree_key='gch',
    graph_and_blk_tree_map_file_key='blks_pickle_url',
    graph_key='g',
    blocks_key='blocks',
    shape_color_map=None,
    legend_map_list=None,
    legend_font_size=None,
    legend_loc='lower center',
    legend_ncol=2,
    edge_style_map={'solid': {'color': '#999999', 'alpha': 0.3, 'width': 0.3},
                    'dashdot': {'color': '#999999', 'alpha': 0.3, 'width': 0.2}},
    edge_style_key='linestyle',
    edge_groups={'solid': [], 'dashdot': []},
    **kwargs):
    """Plot a set of hierarchy blocks.

    This function has a general interface so that we can plot
    graphs grouped under a category (say region) and further
    divided into time periods.
    Each item in the set is expected to be a dict object.
    We expect each item to point to a file url where
    where the graph and block tree objects for a particular
    graph are stored, e.g., graph and block tree for China
    between 1992 and 1995.

    :param graph_and_blk_tree_set: items to plot; each item must include items graph_and_blk_tree_key, 'region', 'interval'
    :param graph_and_blk_tree_set_file: optionally, load items from file
    :param prefix: plot file prefix (with terminal slash for directory)
    :param subtitle: add subtitle to each plot
    :param preprocessors: list of functions to apply to graph_and_blk_tree items (e.g. adding graph attributes etc)
    :param verbose: verbosity level
    :param title: if given, title generation is skipped
    :param group: restrict set to group
    :param subgroup_from: restrict set to a subgroup starting at `from`, inclusive
    :param subgroup_to: restrict set to a subgroup ending at `to`, inclusive
    :param group_key: group selector in the dict object, e.g., `region`
    :param subgroup_key: subgroup selector in the dict object, e.g., `interval`
    :param graph_and_blk_tree_key: graph and block tree object selector
    :param graph_and_blk_tree_map_file_key: graph and block tree file url selector
    :param graph_key: graph object selector
    :param blocks_key: blocks objector selector
    :param shape_color_map: dict of {shape:color}
    :param legend_map_list: a list of legend attribute dicts
    :param legend_font_size: legend font size, if different from node font size
    :param legend_ncol: num of columns in legend
    :param edge_style_map: a dict of dicts for styling edges
    :param edge_style_key: selector for edge_style_map
    :param edge_groups: group of edges based on edge_style_key
    :param **kwargs: additional arguments passed to loading function, plotting function, etc.
    """
    # check if we got graph_and_blk_tree_set, file, or abomination
    if graph_and_blk_tree_set_file is None and graph_and_blk_tree_set is None:
        raise ValueError("Must provide either graph_and_blk_tree_set or graph_and_blk_tree_set_file argument")
    elif graph_and_blk_tree_set_file is not None and graph_and_blk_tree_set is not None:
        warn("Both graph_and_blk_tree_set and graph_and_blk_tree_set_file provided: using graph_and_blk_tree_set.")
    if graph_and_blk_tree_set is None:
        graph_and_blk_tree_set = load_block_set(graph_and_blk_tree_set_file,
                                                group_key=group_key,
                                                group=group,
                                                subgroup_key=subgroup_key,
                                                subgroup_from=subgroup_from,
                                                subgroup_to=subgroup_to,
                                                graph_and_blk_tree_map_file_key=graph_and_blk_tree_map_file_key,
                                                graph_and_blk_tree_key=graph_and_blk_tree_key)
    # sensible defaults
    verbose = verbose if verbose is not None else 0
    prefix = './' if prefix is None else prefix
    # preprocessors
    for prep in preprocessors:
        graph_and_blk_tree_set = map(prep, graph_and_blk_tree_set)
    # plot items
    for graph_and_blk_tree_map in graph_and_blk_tree_set:
        # get id and title
        graph_and_blk_tree_id = '%s_%d_%d' % (graph_and_blk_tree_map[group_key],
                                            graph_and_blk_tree_map[subgroup_key].start,
                                            graph_and_blk_tree_map[subgroup_key].end)
        if title is None:
            graph_and_blk_tree_title = '%s (%d-%d)' % (graph_and_blk_tree_map[group_key],
                                                    graph_and_blk_tree_map[subgroup_key].start,
                                                    graph_and_blk_tree_map[subgroup_key].end)
        else:
            graph_and_blk_tree_title = title
        if verbose > 0:
            print(graph_and_blk_tree_id)
        graph_and_blk_tree = graph_and_blk_tree_map[graph_and_blk_tree_key]
        # add a subtitle if any is given
        if subtitle is not None:
            graph_and_blk_tree_title += '\n%s' % subtitle
        # run block plot
        try:
            block_plot(graph_and_blk_tree,
                title=graph_and_blk_tree_title,
                outfile='%s%s-out.pdf' % (prefix, graph_and_blk_tree_id),
                verbose=verbose,
                shape_color_map=shape_color_map,
                bbox=bbox,
                legend_map_list=legend_map_list,
                legend_font_size=legend_font_size,
                legend_ncol=legend_ncol,
                legend_loc=legend_loc,
                edge_groups=edge_groups,
                edge_style_key=edge_style_key,
                edge_style_map=edge_style_map,
                **kwargs)
        except:
            print(traceback.format_exc())
    # duplicate block statistics if verbose
    if verbose > 0:
        block_statistics = OrderedDict()
        for graph_and_blk_tree_map in graph_and_blk_tree_set:
            graph_and_blk_tree_id = '%s_%d_%d' % (graph_and_blk_tree_map[group_key],
                                graph_and_blk_tree_map[subgroup_key].start,
                                graph_and_blk_tree_map[subgroup_key].end)
            graph_and_blk_tree = graph_and_blk_tree_map[graph_and_blk_tree_key]
            block_count = Counter()
            for v in graph_and_blk_tree[graph_key].vs:
                l = len(v[blocks_key])
                block_count[l] += 1
            block_statistics[graph_and_blk_tree_id] = block_count
        print('Multiple Block Membership:')
        for (k,v) in block_statistics.items():
            print('%s: %s' % (k, sorted(v.items(), key=lambda x: x[0])))

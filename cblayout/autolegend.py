from matplotlib.lines import Line2D


def make_legend_patches_from_attribute(g,
    _type=None,
    values=None,
    colors=None,
    shapes=None,
    linewidth=1,
    fontsize=10,
    markersize=10):
    """Make a patch for each value of the attribute by usig matplotlib.

    :param _type: type of the patch, accepts 'line' or 'marker'
    :param values: a list of attribute values
    :param shapes: shapes, if any, corresponding to values
    :param linewidth: linewidth if we draw a line
    :param fontsize: currently not used, as it is set by ax.legend
    :param markersize: the size of the marker shape in patch
    :return: list of patches

    ..note:
        If the type is 'marker', we erase the line
        by using color white for the line.
        Simplifies the code, since we can use Line2D,
        but will fail if the background of the figure is not white.
    """
    _patches = []
    if _type is 'line':
        for v, c, s in zip(values, colors, shapes):
            p = Line2D([0], [0],
                color=c,
                lw=linewidth,
                label=v,
                linestyle=s)
            _patches.append(p)
    elif _type is 'marker':
        for v, c, s in zip(values, colors, shapes):
            p = Line2D([0], [0],
                marker=s,
                color='w',  # hack, assumes that the background is white
                label=v,
                markerfacecolor=c,
                markersize=markersize)
            _patches.append(p)
    return _patches

import re
import matplotlib.pyplot as plt
import igraph as ig
import networkx as nx
import numpy as np
from descartes import PolygonPatch
from matplotlib.collections import PatchCollection
from collections import defaultdict
from warnings import warn


def node_attribute_or_default(nxg, attribute, default):
    """Return a dict of {node: attribute value}
    where the value is default if attribute value does not exist for the node.
    Otherwise return the default value itself.

    :param nxg: networkx graph object
    :param attribute: name of attribute
    :param default: default value for attribute
    :return: dict of node and attribute values
    """
    # get node attributes by name of attribute
    # key is node
    d = nx.get_node_attributes(nxg, attribute)
    if len(d) == 0:
        # empty, return the default
        return default
    else:
        res = dict()
        # go through all the nodes
        for node in nxg.nodes():
            if (node in d) and (d[node] is not None):
                # attribute exists
                res[node] = d[node]
            else:
                # absent attribute, update with default
                res[node] = default
        return res


def edge_attribute_or_default(nxg, attribute, default):
    """Return a list of edge attribute values
    where the value is default if attribute does not exist.

    :param nxg: networkx graph object
    :param attribute: name of attribute
    :param default: default value for attribute
    :return: list of edge attribute values
    """
    # get node attributes by name of attribute
    # key is edge tuple
    # color=nx.get_edge_attributes(G,'color')
    # color[(1,2)] -> 'red'
    d = nx.get_edge_attributes(nxg, attribute)
    if len(d) == 0:
        return default
    else:
        res = list()
        for u in nxg.edges():
            # note that edges() returns a list of tuples 
            if (u in d) and (d[u] is not None):
                res.append(d[u])
            else:
                res.append(default)
        return res


def set_defaults(nxg,
    kws={'nodes': '_nodes_',
        'edges': '_edges_',
        'labels': '_labels_',
        'hierarchy': '_hierarchy_'},
    _node_size=['node_size', 100],
    _node_color=('node_color', '#a6cee3'),
    _node_shape=('node_shape', 'o'),
    _edge_color=('edge_color', '#999999'),
    _font_size=('font_size', 8),
    _horizontalalignment=('horizontalalignment', 'center'),
    _verticalalignment=('verticalalignment', 'center'),
    _edges_alpha=('_edges_alpha', 0.33),
    _edges_width=('_edges_width', 0.5),
    **kwargs):
    """Set defaults for the networkx graph.

    Duplicates kwargs for edges, labels, etc., except
    parameters marked by kw such as _edges_
    Needed bec some kwargs overlap btw draw_networkx_edges,
    draw_networkx_nodes etc.
    A bit of a kludge, but works fine.

    :param nxg: networkx graph object
    :param kws: dict for component keywords
    :param _node_size:
    :param _node_color:
    :param _node_shape:
    :param _edge_color:
    :param font_size:
    :param _horizontalalignment:
    :param _verticalalignment:
    :param _edges_alpha:
    :param _edges_width:
    :param *kwargs: kwargs to be updated
    :return: tuple (kwargs, nkwargs, ekwargs, lkwargs, hkwargs)
    """
    def extract_kwargs(kwargs, kw):
        """Replace only the ones starting with kw."""
        kwargs_copy = kwargs.copy()
        for k in kwargs.keys():
            if k.startswith(kw):
                del kwargs_copy[k]
                kwargs_copy[ k[len(kw):] ] = kwargs[k]
        return kwargs_copy

    # use dict setdefault method to set reasonable defaults if no value given
    kwargs.setdefault(_node_size[0],
                    node_attribute_or_default(nxg, _node_size[0], _node_size[1]))
    kwargs.setdefault(_node_color[0],
                    node_attribute_or_default(nxg, _node_color[0], _node_color[1]))
    kwargs.setdefault(_node_shape[0],
                    node_attribute_or_default(nxg, _node_shape[0], _node_shape[1]))
    kwargs.setdefault(_edge_color[0],
                      edge_attribute_or_default(nxg, _edge_color[0], _edge_color[1]))
    kwargs.setdefault(_font_size[0], _font_size[1])
    kwargs.setdefault(_horizontalalignment[0], _horizontalalignment[1])
    kwargs.setdefault(_verticalalignment[0], _verticalalignment[1])
    kwargs.setdefault(_edges_alpha[0], _edges_alpha[1])
    kwargs.setdefault(_edges_width[0], _edges_width[1])

    # extract kwargs specific to nodes, edges, etc
    # these are marked by the value of kws['nodes'] etc
    nkwargs = extract_kwargs(kwargs, kws['nodes'])
    ekwargs = extract_kwargs(kwargs, kws['edges'])
    lkwargs = extract_kwargs(kwargs, kws['labels'])
    hkwargs = extract_kwargs(kwargs, kws['hierarchy'])

    return kwargs, nkwargs, ekwargs, lkwargs, hkwargs


def get_figure(title='Cohesive Block Plot',
    xlim=[-1,1],
    ylim=[-1,1],
    figsize=(10,10),
    *args,
    **kwargs):
    """Scaffold the matplotlib figure.

    :param xlim:
    :param ylim:
    :param figsize:
    :param *args: args to be passed to matplotlib
    :param *kwargs: kwargs to be passed to matplotlib
    :return: tuple of figure and axes objects
    """
    fig, ax = plt.subplots(1, 1, figsize=figsize)
    # set the limits
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    # don't need the axes
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    ax.axis('off')
    # add title
    fig.suptitle(title)
    # tight_layout gets rid of some margins
    fig.tight_layout()
    return fig, ax


def draw_hierarchy(hierarchy,
    ax,
    use_shade=True,
    shade_step_size=3,
    shade_global_darken=0,
    shade_edge_color_darken=40,
    shade_label_darken=120,
    label_hierarchy=True,
    levels=None,
    *args, **kwargs):
    """Draw the nested hierarchy by using matplotlib.

    :param ax: axes object
    :param use_shade: use shading?
    :param shade_step_size:
    :param shade_global_darken:
    :param shade_edge_color_darken:
    :param shade_label_darken:
    :param label_label_hierarchy:
    :param levels: a set of levels to draw, default draws all levels
    :return: ax updated with the appropriate patches
    """
    def shade(steps, step_size=shade_step_size, darken=0):
        """Return shade hex code"""
        return '#%02x%02x%02x' % tuple([(255 - darken) - (steps * step_size)] * 3)

    # draw hierarchy patches and decorate
    patches = []

    for hv in hierarchy.vs:
        # filter by the level set
        if levels is not None and hv['_klevel'] not in levels:
            continue
        # load block space
        shape = hv['_shape']
        klev = hv['_klevel']
        if use_shade:
            fc = shade(klev, darken=shade_global_darken)
            ec = shade(klev, darken=shade_edge_color_darken+shade_global_darken)
        else:
            # TODO: ponder this
            fc = shade(0, darken=shade_global_darken)
            ec = shade(0, darken=shade_edge_color_darken+shade_global_darken)
        if shape.geom is not None:
            patches.append(PolygonPatch(shape.geom, fc=fc, ec=ec, lw=0.5))

        # load self space for label positioning
        # and maybe debugging?
        space = hv['_self_space']
        if space is not None:
            (x0, y0, x1, y1) = space.bbox
            coord = (x0, y0)
        else:
            coord = None

        # add labels
        if label_hierarchy:
            if use_shade:
                labelc = shade(klev, darken=shade_label_darken+shade_global_darken)
            else:
                labelc = shade(0, darken=shade_label_darken+shade_global_darken)
            if 'label' in hierarchy.vertex_attributes():
                if coord is not None:
                    ax.text(coord[0], coord[1], hv['label'],
                            color=labelc, fontsize=kwargs['font_size'],
                            ha='left',va='bottom')
    patch_coll = PatchCollection(patches, match_original=True)
    ax.add_collection(patch_coll)
    return ax

def sanitize_labels(lbls):
    """Get rid of co, ltd, inc, etc in company name.

    TODO: get rid of hardcoding?

    :param: a dict of labels where the key is the node
    :return: updated dict of labels
    """
    # note that we do the cleaning in steps
    # to avoid chopping words like 'corporate'
    for k, v in lbls.items():
        # get rid of '& Co'
        v = re.sub(r'\s+&\s+co\s+', ' ', v, flags=re.I)
        # 'ltd' 'co' 'llc' etc
        # do it a couple of times bec some firms are like 'co ltd'
        v = re.sub(r'\s+(co|ltd|llc|inc|pty)\s+', ' ', v, flags=re.I)
        v = re.sub(r'\s+(co|ltd|llc|inc|pty)\s+', ' ', v, flags=re.I)
        v = re.sub(r'\s+(co|ltd|llc|inc|pty)\s+', ' ', v, flags=re.I)
        v = re.sub(r'\s+(co|ltd|llc|inc|pty)\s+', ' ', v, flags=re.I)
        v = re.sub(r'\s+(co|ltd|llc|inc|pty)\s+', ' ', v, flags=re.I)
        # get rid of any dangling junk at the end of the string
        v = re.sub(r'\s+(co|ltd|llc|inc|pty|&|\.)$', '', v, flags=re.I)
        v = re.sub(r'\s+(co|ltd|llc|inc|pty|&|\.)$', '', v, flags=re.I)
        # just in case
        v = v.strip()
        # assign it as label
        lbls[k] = v
    return lbls


def plot(nxg,
    hierarchy,
    pos,
    outfile='out.pdf',
    legends=None,
    plot_edges=True,
    plot_nodes=True,
    plot_labels=True,
    levels=None,
    legend_loc=None,
    legend_font_size=None,
    legend_ncol=None,
    shape_color_map=None,
    edge_style_map=None,
    edge_style_key=None,
    edge_groups=None,
    **kwargs):
    """Plot by using networkx (backed by matplotlib).

    :param nxg: underlying graph
    :param hierarchy: hierarchy tree
    :param pos: node positions
    :param outfile: output filepath
    :param legends: a list of legend patches
    :param plot_edges: whether to plot edges
    :param plot_nodes: whether to plot nodes
    :param plot_labels: whether to plot labels
    :param levels: set of levels to draw
    :param legend_loc: location of the legend
    :param legend_font_size: legend font size, if different from node font size
    :param legend_ncol: num of columns in legend
    :param shape_color_map: dict of {shape:color}
    :param edge_style_map: a dict of dicts for styling edges
    :param edge_style_key: selector for edge_style_map
    :param edge_groups: group of edges based on edge_style_key
    :param *kwargs: various kwargs to pass to networkx draw funcs
    """
    # set defaults
    kwargs, nkwargs, ekwargs, lkwargs, hkwargs = set_defaults(nxg, **kwargs)
    fig, ax = get_figure(**kwargs)
    # make legend
    # first find if legend has a different font size
    if legend_font_size is not None:
        _fontsize = legend_font_size
    else:
        _fontsize = nkwargs['font_size']
    if legends is not None:
        # legends is a list of patches
        # add them to the figure legend
        ax.legend(handles=legends,
            ncol=legend_ncol,
            loc=legend_loc,
            fontsize=_fontsize,
            labelspacing = _fontsize/4,
            borderpad=1.0,
            frameon=True)

    # here create a new networkx graph based on levels
    # new_graph()
    draw_hierarchy(hierarchy, ax=ax, **hkwargs)

    # draw graph
    if plot_edges:
        if edge_style_key is not None:
            for e in nxg.edges():
                u = e[0]
                v = e[1]
                _style = nxg[u][v][edge_style_key]
                edge_groups[_style].append((u, v))
            for _style, group in edge_groups.items():
                sg = nx.Graph()
                sg.add_edges_from(group)  
                nx.draw_networkx_edges(sg,
                    pos=pos,
                    style=_style,
                    alpha=edge_style_map[_style]['alpha'],
                    color=edge_style_map[_style]['color'],
                    width=edge_style_map[_style]['width'],
                    ax=ax)
        else:
            # or draw with defaults
            # TODO: having two styling systems is bad. how would defaults look in the edge_style_map system?
            nx.draw_networkx_edges(nxg,
                pos=pos,
                ax=ax,
                **ekwargs)
    if plot_nodes:
        # set _nattr to node shape attribute
        _nattr = nkwargs['node_shape']
        # if it is a dict, we have different shapes
        # for each class of nodes
        if isinstance(_nattr, dict):
            # find the unique values for shapes
            unqk = set(_nattr.values())
            # iterate thru unique values
            for k in unqk:
                # get node positions for nodes with this shape
                tpos = {n:v for (n,v) in pos.items() if _nattr[n] == k}
                # get list of nodes with this shape
                nodelist = tpos.keys()
                # set node_shape to a single value
                # originals stored in _nattr dict
                nkwargs['node_shape'] = k
                # now get the color for this shape
                if shape_color_map is not None:
                    if k in shape_color_map:
                        nkwargs['node_color'] = shape_color_map[k]
                    else:
                        warn('Achtung: no color for shape {}'.format(k))
                else:
                    # use default but also issue a warning
                    warn('Dikkat: Using default node color although we have multiple shapes.')
                # now ready to draw the network for this class of nodes
                nx.draw_networkx_nodes(nxg, pos=tpos, ax=ax,
                                       nodelist=nodelist, **nkwargs)
        else:
            # no distinct shapes, just draw with default node_shape
            nx.draw_networkx_nodes(nxg, pos=pos, ax=ax, **nkwargs)
    if plot_labels:
        lkwargs['labels'] = sanitize_labels(lkwargs['labels'])
        nx.draw_networkx_labels(nxg, pos=pos, ax=ax, **lkwargs)
    # save the figure
    fig.savefig(outfile)
    plt.close()

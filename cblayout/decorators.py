import re
import igraph as ig
from matplotlib.cm import get_cmap
from collections import defaultdict
from .block_class import Block


def swap_ordered_pair(t):
    """Swap the elements in a tuple or list.

    >>> swap_ordered_pair([1, 2])
    [2, 1]
    >>> swap_ordered_pair([3, 4])
    [4, 3]
    >>> swap_ordered_pair((3, 5))
    (5, 3)
    """
    r = []
    r.append(t[1])
    r.append(t[0])
    if isinstance(t, list):
        return r
    if isinstance(t, tuple):
        return tuple(r)

############################################################
# Graph Decorators

def graph_add_vertex_attributes(g, attribute_dict):
    """Add attributes to a vertex if vertex is in attribute_dict.

    :param g: igraph Graph object.
    :param attribute_dict: dictionary of attributes keyed by vertex name.
    :return: Graph object itself.
    """
    for v in g.vs:
        name = v['name']
        if name in attribute_dict:
            v.update_attributes(attribute_dict[name])
    return g


def graph_add_edge_attributes(g, attribute_dict):
    """Add attributes to an edge if edge tuple is in attribute_dict.

    :param g: igraph Graph object.
    :param attribute_dict: dictionary of attributes keyed by edge tuple.
    :return: Graph object itself. 
    """
    if g.is_directed():
        for e in g.es:
            t = e.tuple
            if t in attribute_dict:
                e.update_attributes(attribute_dict[t])
    else:
        for e in g.es:
            t = e.tuple
            t2 = swap_ordered_pair(e.tuple)
            if (t in attribute_dict) or (t2 in attribute_dict):
                e.update_attributes(attribute_dict[t])
    return g


def graph_vertex_shape_from_attribute(g, attribute, shapes=None, default='o'):
    """Set shapes for attribute values.

    Uses either a default shapemap or given shapes.
    Works with `matplotlib.markers` used by `Axes.scatter`.

    :param g: igraph Graph object
    :param attribute: attribute name
    :param shapes: shapemap, a dict {attribute_val: shape}
    :param default: default shape
    :return: updated igraph Graph object
    """
    sm = 'ovxXDd+^<>.,12348spP*hH|_'  # default shapemap
    # check whether shapes are supplied
    if shapes is None:
        # no shapes are given, start with an empty dict
        _shapes = {}
        nshapes = 0
    else:
        _shapes = shapes
    # if attribute exists in the Graph, fetch its value
    # otherwise assign 'default' to the value
    attribute_exists = attribute in g.vertex_attributes()
    for v in g.vs:
        if attribute_exists:
            val = v[attribute]
        else:
            val = 'default'
        # set shape automatically if we work with default shapemap
        if shapes is None and val not in _shapes:
            # first check if we ran out of shapes
            if nshapes >= len(sm):
                raise ValueError('Attribute takes on too many values for shapemap')
            _shapes[val] = sm[nshapes % (len(sm))]
            nshapes += 1
        # we work with user supplied shapemap
        # check whether we have shapes for all the attribute values
        elif (val != 'default') and (val not in _shapes.keys()):
            raise IndexError('No shape supplied for attribute value `%s`' % val)
        # set shape
        if val == 'default':
            v['node_shape'] = default
        else:
            v['node_shape'] = _shapes[val]
    return g

def graph_make_multiple_block_vertices(g0,
    blocks_attr='blocks',
    attr_lbls=None):
    """
    Duplicate vertices that belong in multiple blocks.

    Creates a new graph with vertex attributes '_multiblock' and '_duplicate':
    '_multiblock' denotes a vertex that belongs in multiple blocks.
    '_duplicate' denotes a vertex that was duplicated.

    :param g0: igraph graph
    :param blocks_attr: graph vertex attribute containing the blocks a vertex belongs to.
    :param attr_lbls: dictionary of values to assign to 'multiblock' and 'other' vertices
    :param
    :return: igraph graph with duplicated vertices
    """
    g = g0.copy()
    if attr_lbls is None:
        attr_lbls = { 'multiblock': True, 'other': False }

    # get edges for later addition
    # es = g.get_adjlist()
    for v in g.vs:
        v['_duplicate'] = None
        if len(v[blocks_attr]) > 1:
            v['_multiblock'] = attr_lbls['multiblock']
        else:
            v['_multiblock'] = attr_lbls['other']

    for v in g.vs:
        es = g.get_adjlist()
        vd = v.attributes()
        vn = vd['name']
        del vd['name']
        # if 'duplicate' not in vd or vd['duplicate'] is None:
        if vd['_duplicate'] is None:
            v['_duplicate'] = False
            vd['_duplicate'] = True
        else:
            continue
        # v['block'] = v['blocks'][0]
        for blk in v[blocks_attr]:
            if blk == v['block']:
                continue
            vd['block'] = blk
            g.add_vertex(name=vn, **vd)
            s = len(g.vs)-1
            g.add_edges([(s, t) for t in es[v.index]])
    return g

def mark_incident_edges_based_on_vertex_attribute(g,
    v_attribute=None,
    v_val=None,
    edge_attribute=None,
    edge_val=None,
    edge_val_default=None):
    """Assign edge attribute values based on attributes of incident vertices.

    :param g: graph
    :param v_attribute: vertex attribute to search for
    :param v_val: vertex attribute value
    :param edge_attribute: edge attribute to assign
    :param edge_val: edge attribute value to assign
    :param edge_val_default: default edge attribute to assign
    """
    # TODO:
    # (1) should we pass a single dictionary of vertex values -> edge values?
    # (2) separate target and source vertices?
    for e in g.es:
        s = e.source
        t = e.target
        if v_attribute in g.vs[s].attribute_names() and g.vs[s][v_attribute] == v_val:
            e[edge_attribute] = edge_val
        elif v_attribute in g.vs[t].attribute_names() and g.vs[t][v_attribute] == v_val:
            e[edge_attribute] = edge_val
        else:
            e[edge_attribute] = edge_val_default
    return g   

############################################################
# GCH Decorators

def filter_gch_by_blocks(gch, block_ids):
    """Filters block-set to display only selected block ids, producing modified graph and hierarchy.

    :param gch: block-set
    :param block_ids: ids of blocks to display
    """
    g = gch['g']
    blks = gch['blks']

    # key step: eliminate missing blocks from the parentage tree
    # new blocks and new hierarchy must reflect changed parentage tree

    # prune good blocks and rewrite parent lists
    children_dict = defaultdict(list)
    for blk in reversed(blks):
        if blk.idx in block_ids:
            # if you're one of the key nodes, we track your parent
            children_dict[blk.parent].append(blk.idx)
        elif blk.idx in children_dict:
            # else if you're a tracked parent, we shorten your edge
            children_dict[blk.parent] = children_dict[blk.idx]
            del children_dict[blk.idx]

    # reverse parent-child dictionary
    parent_dict = dict()
    for k, cs in children_dict.items():
        for c in cs:
            parent_dict[c] = k

    # make new blocks list
    blks2 = list()
    blk_id_map = dict()
    index = 0
    for blk_id in sorted(block_ids):
        blk = blks[blk_id]
        # rewrite parent
        parent = parent_dict[blk_id]
        parent = blk_id_map[parent] if parent is not None else None
        blk2 = Block(idx=index, parent=parent,
                vertices=blk.vertices.copy(), numelem=0,
                klevel=blk.klevel, kconn=blk.kconn)
        blk2.numelem = len(blk2.vertices)
        # register new block id
        blk_id_map[blk_id] = index
        index += 1
        blks2.append(blk2)

    # make new hierarchy
    hier2 = ig.Graph()
    hier2.add_vertices(len(blks2))
    for blk in blks2:
        if blk.parent is not None:
            hier2.add_edge(blk.parent, blk.idx)

    # make new graph: prune excluded vertices from graph
    keep_vs = [v.index for v in g.vs if v['block'] in block_ids]
    g2 = g.induced_subgraph(keep_vs)
    for v in g2.vs: v['block'] = blk_id_map[v['block']]

    return { 'g': g2, 'blks': blks2, 'hier': hier2 }

def blocks_at_level(gch, levels):
    """
    Identifies blocks at a given depth level.

    :param gch: gch structure
    :param levels: list of levels to find
    """
    return [blk.idx for blk in gch['blks'] if blk.klevel in levels]

def filter_gch_by_block_levels(gch, levels):
    """
    Wrapper around `filter_gch_by_blocks` to filter blocks at a given depth level.

    :param gch: gch structure
    :param levels: list of levels to find
    """
    return filter_gch_by_blocks(gch, blocks_at_level(gch, levels))

############################################################
# GCHItem Decorators

############################################################
# Generalized Decorators

def _graph_to_gch_decorator(f0):
    """
    Wrapper to turn graph decorators into gch decorators.

    >>> f_gch = _graph_to_gch_decorator(f_graph)
    >>> f_gch(gch, arguments_to_f_graph)
    """
    def f1(gch, *args, **kw_args):
        g = gch['g']
        gch['g'] = f0(g, *args, **kw_args)
        return gch
    return f1


def _graph_to_gchitem_decorator(f0):
    """
    Wrapper to turn graph decorators into gchitem decorators.

    >>> f_gchitem = _graph_to_gchitem_decorator(f_graph)
    >>> f_gchitem(gchitem, arguments_to_f_graph)
    """
    def f1(gchitem, *args, **kw_args):
        g = gchitem['gch']['g']
        gchitem['gch']['g'] = f0(g, *args, **kw_args)
        return gchitem
    return f1


def _gch_to_gchitem_decorator(f0):
    """
    Wrapper to turn gch decorators into gchitem decorators.

    >>> f_gchitem = _gch_to_gchitem_decorator(f_gch)
    >>> f_gchitem(gchitem, arguments_to_f_gch)
    """
    def f1(gchitem, *args, **kw_args):
        gch = gchitem['gch']
        gchitem['gch'] = f0(gch, *args, **kw_args)
        return gchitem
    return f1

############################################################
# Convenience

add_vertex_attributes = _graph_to_gchitem_decorator(graph_add_vertex_attributes)

vertex_shape_from_attribute = _graph_to_gchitem_decorator(graph_vertex_shape_from_attribute)

make_multiple_block_vertices = _graph_to_gchitem_decorator(graph_make_multiple_block_vertices)
